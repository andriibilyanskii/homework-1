const createFile = require('./functions/createFile');
const getFile = require('./functions/getFile');
const getFiles = require('./functions/getFiles');
const editFile = require('./functions/editFile');
const deleteFile = require('./functions/deleteFile');
const createLog = require('./logs');

const fs = require('fs');

const express = require('express');
const app = express();
app.use(express.json());

const cors = require('cors');
app.use(cors());

const dirFiles = './files';
const dirLogs = './logs';

if (!fs.existsSync(dirFiles)) {
	fs.mkdirSync(dirFiles, { recursive: true });
}

if (!fs.existsSync(dirLogs)) {
	fs.mkdirSync(dirLogs, { recursive: true });
}

app.use((req, res, next) => {
	createLog(req, res, dirLogs);
	res.setHeader('Content-Type', 'application/json');
	next();
});

app.post('/api/files', (req, res) => {
	createFile(req, res, dirFiles);
});

app.get('/api/files', (req, res) => {
	getFiles(req, res, dirFiles);
});

app.get('/api/files/:filename', (req, res) => {
	getFile(req, res, dirFiles);
});

app.put('/api/files/:filename', (req, res) => {
	editFile(req, res, dirFiles);
});

app.delete('/api/files/:filename', (req, res) => {
	deleteFile(req, res, dirFiles);
});

app.listen(8080);
