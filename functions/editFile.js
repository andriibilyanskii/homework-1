const fs = require('fs');

function editFile(req, res, dirFiles) {
	let filename = req.params.filename;

	try {
		let files = fs.readdirSync(dirFiles);
		if (!files.includes(filename)) {
			res.status(400).send({
				message: `No file with '${filename}' filename found`
			});
			return;
		}

		fs.writeFile(`${dirFiles}/${filename}`, req.body.content, (error) => {
			if (error) {
				throw new Error(error);
			}
			res.status(200).send({
				message: 'File edited successfully'
			});
		});
	} catch (e) {
		res.status(500).send({
			message: 'Server error'
		});
	}
}

module.exports = editFile;