const fs = require('fs');
const path = require('path');

function getFile(req, res, dirFiles) {
	const filename = req.params.filename;
	
	try {
		let files = fs
			.readdirSync(dirFiles)
			.filter((e) =>
				path.extname(`${dirFiles}/${e}`).match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)
			);

		if (!files.includes(filename)) {
			res.status(400).send({
				message: `No file with '${filename}' filename found`
			});
			return;
		}

		let fileContent = fs.readFileSync(`${dirFiles}/${filename}`, 'utf8');

		res.send({
			message: 'Success',
			filename: filename,
			content: fileContent,
			extension: path.extname(`${dirFiles}/${filename}`).replace('.', ''),
			uploadedDate: fs.statSync(`${dirFiles}/${filename}`).mtime
		});
	} catch (e) {
		res.status(500).send({
			message: `Server error`
		});
	}
}

module.exports = getFile;
