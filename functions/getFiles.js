const fs = require('fs');
const path = require('path');

function getFiles(req, res, dirFiles) {
	try {
		let files = fs
			.readdirSync(dirFiles)
			.filter((e) =>
				path.extname(`${dirFiles}/${e}`).match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)
			);
			
		if (files.length !== 0) {
			res.status(200).send({
				message: 'Success',
				files: files
			});
		} else {
			res.status(400).send({
				message: 'Client error'
			});
		}
	} catch (e) {
		res.status(500).send({
			message: 'Server error'
		});
	}
}

module.exports = getFiles;