const fs = require('fs');

function createFile(req, res, dirFiles) {
	let filename = req.body.filename;

	try {
		if (!filename) {
			throw 'filename';
		} else if (!req.body.content) {
			throw 'content';
		}
	} catch (e) {
		res.status(400).send({
			message: `Please specify ${e} parameter`
		});
		return;
	}
		
	try {
		let files = fs.readdirSync(dirFiles);
		if (files.includes(filename)) {
			res.status(500).send({
				message: 'This file already exists'
			});
			return;
		}

		if (!filename.match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)) {
			throw Error();
		}

		if (!fs.existsSync(dirFiles)) {
			fs.mkdirSync(dirFiles);
		}

		fs.writeFile(`${dirFiles}/${filename}`, req.body.content, (error) => {
			if (error) {
				throw new Error(error);
			}
			res.status(200).send({
				message: 'File created successfully'
			});
		});
	} catch (e) {
		res.status(500).send({
			message: 'Server error'
		});
	}
}

module.exports = createFile;
