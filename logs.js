const fs = require('fs');

function createLog(req, res, dirLogs) {
	let log = `${req.method}\t${req.path}\t${new Date().toISOString()}`;
	if (req.method === 'PUT' || req.method === 'POST') {
		log += '\t' + JSON.stringify(req.body);
	}

	console.log(log);

	fs.appendFile(`${dirLogs}/logs.log`, `${log}\n`, (error) => {
		if (error) {
			throw new Error(error);
		}
	});
}

module.exports = createLog;
